#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>


#define MAX_COLUMNS 29
#define MAX_LINE_LENGTH 1000

void readCSV(const char* filename)
{
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("File not found!\n");
        return;
    }

    // Read the header line
    char header[MAX_LINE_LENGTH];
    fgets(header, sizeof(header), file);

    // Read and print each line of the CSV file
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), file))
    {
        // Tokenize the line by comma
        char* token = strtok(line, ",");
        int column = 1; // Starting from column 1 instead of 0
        int id = 0;
        char name[100];
        int age = -1;
        char photoURL[100];
        char nationality[100];
        char flag[100];
        int overall = -1;
        int potential = -1;
        char club[100] = ""; // Initialize club with an empty string
        char clublogo[100];
        char value[100];
        char wage[100];
        int special = -1;
        char foot[100];
        double rep = -1;
        double weakfoot = -1;
        double skill = -1;
        char work[100];
        char type[100];
        char face[100];
        char position[100];
        char joind[100];
        char joiny[100];
        char loan [100];
        int cont = 0;
        char height[100];
        char weight[100];
        char release[100];
        double kit = -1;
        char rate[100];

        while (token != NULL && column <= MAX_COLUMNS)
        {
            if (column == 1)
            { // id column
                id = atoi(token); // Convert the id token to an integer
            }
            if (column == 2)
            { // name column
                strncpy(name, token, sizeof(name) - 1);
            }
            if (column == 3)
            { // Age column
                age = atoi(token); // Convert the age token to an integer
            }
            if (column == 4)
            { // photo column
                strncpy(photoURL, token, sizeof(photoURL) - 1);
            }
            if (column == 5)
            { // nationality column
                strncpy(nationality, token, sizeof(nationality) - 1);
            }
            if (column == 6)
            { // flag column
                strncpy(flag, token, sizeof(flag) - 1);
            }
            if (column == 7)
            { // overall column
                overall = atoi(token); // Convert the age token to an integer
            }
            if (column == 8)
            { // Potential column
                potential = atoi(token);
            }
            if (column == 9)
            { // Club column
                strncpy(club, token, sizeof(club) - 1);
            }
            if (column == 10)
            { // club logo column
                strncpy(clublogo, token, sizeof(clublogo) - 1);   
            }
            if (column == 11)
            {
                strncpy(value, token, sizeof(value) - 1);   
            }
            if (column == 12)
            {
                strncpy(wage, token, sizeof(wage) - 1);   
            }
            if (column == 13)
            {
                special = atoi(token);   
            }
            if (column == 14)
            {
                strncpy(foot, token, sizeof(foot) - 1);   
            }
            if (column == 15)
            {
                rep = atoi(token);   
            }
            if (column == 16)
            {
                weakfoot = atoi(token);
            }
            if (column == 17)
            {
                skill = atoi(token);
            }
            if (column == 18)
            {
                strncpy(work, token, sizeof(work) - 1);
            }
            if (column == 19)
            {
                strncpy(type, token, sizeof(type) - 1);
            }
            if (column == 20)
            {
                strncpy(face, token, sizeof(face) - 1);
            }
            if (column == 21)
            {
                strncpy(position, token, sizeof(position) - 1);
            }
            if (column == 22)
            {
                strncpy(joind, token, sizeof(joind) - 1);
            }
            if (column == 23)
            {
                strncpy(joiny, token, sizeof(joiny) - 1);
            }
            if (column == 24)
            {
                strncpy(loan, token, sizeof(loan) - 1);
            }
            if (column == 25)
            {
                cont = atoi(token);
            }
            if (column == 26)
            {
                strncpy(height, token, sizeof(height) - 1);
            }
            if (column == 27)
            {
                strncpy(weight, token, sizeof(weight) - 1);
            }
            if (column == 28)
            {
                strncpy(release, token, sizeof(release) - 1);
            }
            if (column == 29)
            {
                kit = atoi(token);
            }
            if (column == 30)
            {
                strncpy(rate, loan, sizeof(rate)-1);
            }
            
            strcpy(rate, loan);

            token = strtok(NULL, ",");
            column++;
        }

        if (age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0)
        {   
            printf("Id: %d\n", id);
            printf("Name: %s\n", name);
            printf("Age: %d\n", age);
            printf("Photo: %s", photoURL);
            printf("Nationality %s", nationality);
            printf("Flag: %s\n", flag);
            printf("Overall: %d\n", overall);
            printf("Potential: %d\n", potential);
            printf("Club: %s\n", club);
            printf("Club logo: %s\n", clublogo);
            printf("Value: %s\n", value);
            printf("Wage: %s\n", wage);
            printf("Special: %d\n", special);
            printf("Club logo: %s\n", clublogo);
            printf("Preffered foot: %s", foot);
            printf("International reputation: %f\n", rep);
            printf("Weak foot: %f\n", weakfoot);
            printf("Skill moves: %f\n", skill);
            printf("Work rate: %s\n", work);
            printf("Body type: %s\n", type);
            printf("Real face: %s\n", face);
            printf("Position: %s\n", position);
            printf("Joined: %s, %s\n", joind, joiny);
            printf("Loaned from: %s\n", loan);
            printf("Contract valid until: %d\n", cont);
            printf("Height: %s\n", height);
            printf("Weight: %s\n", weight);
            printf("Release clause: %s\n", release);
            printf("Kit number: %f\n", kit);
            printf("Best overall rating: %s\n", rate);
            printf("----------------------------------\n");
        }
    }

    fclose(file);
}

int main()
{
    pid_t pid1;
    int status;
    if ((pid1 = fork()) < 0) 
    {
        perror("Error: Fork failed");
        exit(1);
    } 
    else if (pid1 == 0)
    {
    const char* downloaddulugasie = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    const char* unzip = "unzip fifa-player-stats-database.zip";
    
    system(downloaddulugasie);

    system(unzip);
    }
    wait(&status);

    readCSV("FIFA23_official_data.csv");
    return 0;
}

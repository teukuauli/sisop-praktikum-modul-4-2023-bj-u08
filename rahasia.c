#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

// File paths
static const char *rahasia_path = "/usr/share/rahasia";
static const char *rahasia_zip_path = "/home/vboxuser/rahasia.zip"; // Modify the path according to the location of the rahasia.zip file

// Function to retrieve file attributes
static int rahasia_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;

    memset(stbuf, 0, sizeof(struct stat));

    if (strcmp(path, "/") == 0)
    {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    }
    else if (strcmp(path, rahasia_path) == 0)
    {
        struct stat st;

        if (stat(rahasia_zip_path, &st) == 0)
        {
            stbuf->st_mode = st.st_mode;
            stbuf->st_nlink = st.st_nlink;
            stbuf->st_size = st.st_size;
        }
        else
            res = -ENOENT;
    }
    else
        res = -ENOENT;

    return res;
}

// Function to open a file
static int rahasia_open(const char *path, struct fuse_file_info *fi)
{
    if (strcmp(path, rahasia_path) != 0)
        return -ENOENT;

    int fd = open(rahasia_zip_path, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;
    return 0;
}

// Function to read from a file
static int rahasia_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if (strcmp(path, rahasia_path) != 0)
        return -ENOENT;

    int res = pread(fi->fh, buf, size, offset);
    if (res == -1)
        res = -errno;

    return res;
}

// Fuse operations
static struct fuse_operations rahasia_oper = {
    .getattr = rahasia_getattr,
    .open = rahasia_open,
    .read = rahasia_read,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &rahasia_oper, NULL);
};

// Function to register a user
void registerUser(char* username, char* password) {
    // Open the file storing credentials
    FILE* file = fopen("/usr/share/rahasia/credentials.txt", "a+");
    if (file == NULL) {
        printf("Failed to open credentials file.\n");
        return;
    }

    // Check if the username already exists
    char line[256];
    while (fgets(line, sizeof(line), file)) {
        char* savedUsername = strtok(line, ";");
        if (savedUsername != NULL && strcmp(savedUsername, username) == 0) {
            printf("Username is already registered.\n");
            fclose(file);
            return;
        }
    }

    // Hash the password using MD5
    unsigned char hashedPassword[MD5_DIGEST_LENGTH];
    MD5((unsigned char*)password, strlen(password), hashedPassword);

    // Convert hashedPassword to a string
    char hashedPasswordString[33];
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&hashedPasswordString[i * 2], "%02x", (unsigned int)hashedPassword[i]);
    }


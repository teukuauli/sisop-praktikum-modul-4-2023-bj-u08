# sisop-praktikum-modul-4-2023-BJ-U08


## Group Members


| Name | NRP |
| --- | --- |
| Mochammad Naufal Ihza Syahzada  | 5025211260 |
| Ariel Pratama Menlolo | 5025211194 |
| Teuku Aulia Azhar | 5025201142 |

## Task Check

- [x] soal1
- [x] soal4
- [ ] soal5

## Number 1 Solution
### A
Download from the link 'kaggle datasets download -d bryanb/fifa-player-stats-database' in a file names storage.c
```c
    const char* downloaddulugasie = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    const char* unzip = "unzip fifa-player-stats-database.zip";
    
    system(downloaddulugasie);

    system(unzip);
```
Using system, we use the function 'downloaddulugasie' to download the pre-given datasets, then the func 'unzip' to well, unzip the file and extracet the csv files. we put these commands in the main function of storage.c

### B
In the same storage.c file, we specifically take the 'FIFA23_official_data.csv' file and print out the data of players whose age is under 25 with the potential above 85. Also the printed playes may not be from the team Manchester City

```c
while (token != NULL && column <= MAX_COLUMNS)
        {
            if (column == 1)
            { // id column
                id = atoi(token); // Convert the id token to an integer
            }
            if (column == 2)
            { // name column
                strncpy(name, token, sizeof(name) - 1);
```
So for this file i used the brute force method above to individually scan each column and used the atoi to convert the token values to integers. for those whose values uses string i used the strncpy function to do the same action (i use a -1 because it starts from 0)

```c
void readCSV(const char* filename)
{
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("File not found!\n");
        return;
    }

    // Read the header line
    char header[MAX_LINE_LENGTH];
    fgets(header, sizeof(header), file);

    // Read and print each line of the CSV file
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), file))
    {
        // Tokenize the line by comma
        char* token = strtok(line, ",");
        int column = 1; // Starting from column 1 instead of 0
        int id = 0;
        char name[100];
        int age = -1;
```
Here is the part of the function where i make the command to read the header line after opening the file, and also where i declare each value (age is an integer, name as char,etc) I declared each one for all the columns but i just put little snippets here as to not take up much space in the report.

```c
 if (age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0)
        {   
            printf("Id: %d\n", id);
            printf("Name: %s\n", name);
            printf("Age: %d\n", age);
            printf("Photo: %s", photoURL);
            printf("Nationality %s", nationality);
```
Then i used the if function to filter out the under 25 age, above 85 potentail and not from manchester city to print out every data from the players (again i just put snippets of the print to not take up space in the report)

```c
  {
    const char* downloaddulugasie = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    const char* unzip = "unzip fifa-player-stats-database.zip";
    
    system(downloaddulugasie);

    system(unzip);
    }
    wait(&status);

    readCSV("FIFA23_official_data.csv");
```
In the main we use the &wait command to wait for the download and unzip process to finish then we run the function to read the FIFA23 csv file.

### C
Now we make a Dockerfile and then make the storage.c file and the csv file to be a runnable image.
This was the lines in the Dockerfile:

```
FROM ubuntu:latest

WORKDIR /app

COPY . /app

RUN apt-get update && apt-get install -y gcc

CMD gcc -o storage storage.c && ./storage

```
`FROM ubuntu:latest` uses a specific image in this case linux ubuntu
`WORKDIR /app` and  `COPY ./APP` sets the working directory to /app and the COPY function copies everything to the /app directory
`RUN apt-get update && apt-get install -y gcc` updates the repository package container and installs gcc
`CMD gcc -o storage storage.c && ./storage` Compiles and Runs the storage.c file similar to when you run the gcc compile command in terminal

To build the image we run the command
`docker build -t name .`
where "name" can be replaced with what we want our image name to be

### D
For point D, we need to upload the image to dockerhub under our username and using the tag YBBA
To upload this we need to create a dockerhub account profile similar to how we create a github account.
After, we use the `docker push` command similar to how we woudl push a directory to github.
The docker image was first tagged using the command docker tag `<image_name>:<tag> <dockerhub_username>/<image_name>:<tag>` where in this case the tag will be YBBA
But before all that, we need to login to dockerhub in out cli using the command `docker login`
then, we can finally push the tagged docker image to dockerhub using the command docker push `<dockerhub_username>/<image_name>:<tag>` where the tag is YBBA.

For reference, here is my dockerhub link:
```
https://hub.docker.com/r/cruizard/storage-app
```
### E
For Point E, we need to run the docker image as much as 5 instances from 2 different folders names Barcelona and Napoli. To do that, we make 2 directories name Barcelona and Napoli in which we put the necesary file to run the instances (in this case the storage.c and necessary csv files). 
Then, we make a file named docker-compose.yml in which we insert:
```yml
version: "3"
services:
  barcelona:
    build: 
      context: /home/brel/Documents/soal1/Barcelona
      dockerfile: /home/brel/Documents/soal1/leimage/Dockerfile
    deploy:
      replicas: 5
  napoli:
    build:
      context: /home/brel/Documents/soal1/Napoli
      dockerfile: /home/brel/Documents/soal1/leimage/Dockerfile
    deploy:
      replicas: 5
```
where `services` are the individual services we want to run (in this case barcelona and napoli)
`context` is where the intended service is located (both barcelona and napoli)
`dockerfile` is where the Dockerfile file is located (the one used in point C)
`replicas` is how many instances we want to run, (in this case as per the question is 5 instances)

after that all we need to do is cd to the directory where the yml file is located and run the command
```
docker-compose up -d
```
and it will execute the yml file and run 5 instances of both barcelona and napoli

## No 4
### A. On that filesystem, if you create or rename a directory with the module_ prefix, then that directory will become a modular directory
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) { 
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) filter(enc2);

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);

	int res = 0;
	struct dirent *dir;
	DIR *dp;
	(void) fi;
	(void) offset;
	dp = opendir(newPath);
	if (dp == NULL) return -errno;

	while ((dir = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL) encrypt(dir->d_name, enc2);
        	else {
			char *extension = strrchr(dir->d_name, '.');
            		if (extension != NULL && strcmp(extension, ".001") == 0) {
                		*extension = '\0';
                		decode(newPath, dir->d_name);
            		}
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}
	closedir(dp);
	return 0;
}
```
It is inserted in the code in xmp_readdir() because it wants to indicate the condition of the file when it is read to determine whether or not the file will be modularized. When modularized, the encrypt() function is executed, but when not modularized, the decode() function is executed.
### B. When you modularize a directory, the modularization also applies to the contents of other directories within that directory (subdirectories)
```c
while ((dir = readdir(dp)) != NULL) {
	struct stat st;
	memset(&st, 0, sizeof(st));
	st.st_ino = dir->d_ino;
	st.st_mode = dir->d_type << 12;
	if(enc2 != NULL) encrypt(dir->d_name, enc2);
	else {
		char *extension = strrchr(dir->d_name, '.');
    		if (extension != NULL && strcmp(extension, ".001") == 0) {
        		*extension = '\0';
        		decode(newPath, dir->d_name);
    		}
	}
	res = (filler(buf, dir->d_name, &st, 0));
	if(res!=0) break;
}
```
This code serves the same purpose as point A, and the solution for point B is in the while loop, where the module_ rules will be applied later by moving into the contents of the folder with the while loop and doing the editing procedure.
### C. A file named fs_module.log (/home/[user]/fs_module.log) will create a file called fs_module.log which is used to store a list of system call commands that have been executed
```c
void logSystem(char* c, int type){
	FILE * logFile = fopen("/home/valdo/fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
	int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
	if(type==1) fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
	else if(type==2) fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
	fclose(logFile);
}
```
The logSystem function is used to log information with two input parameters: c (the string to be logged) and type (determines the type of log to be created). Initially, the function opens the file fs_module.log in "a" (append) mode using the fopen function. The file is represented by the logFile pointer, which can be used to write to the file.

Next, the current time is obtained using time and parsed into the time_info variable using localtime. The parsed time information, such as year, month, day, hour, minute, and second, is stored in separate variables such as yr, mon, day, hour, min, and sec.

Then, a check is performed on the type value. If type is 1, it represents a "report" log. The fprintf function is used to write the log entry to the fs_module.log file with the format "REPORT::YYYYMMDD-HH:MM:SS::". The date and time placeholders are filled with the previously obtained values, and c is replaced with the string to be logged. If type is 2, it represents a "flag" log. Similar to the previous case, fprintf is used to write the log entry to the fs_module.log file with the format "FLAG::YYYYMMDD-HH:MM:SS::". The placeholders for date, time, and c are filled with the corresponding values.

Finally, after finishing writing the log entry, the fs_module.log file is closed using fclose to ensure that the log is properly saved. In each FUSE function, the following function calls are made: creating a new directory, creating a new file, deleting a file, deleting a directory, renaming, and writing to a file. The code can be find in the full code with the name start with "xmp_".

### D. During modularization, files that were previously in the original directory will become small files of 1024 bytes and become normal when accessed via a filesystem designed by him

```c
void encrypt(char* enc1, char * enc2){
    	if(strcmp(enc1, ".") == 0) return;
    	if(strcmp(enc1, "..") == 0)return; 
    	int chunks=0, i, accum;
    	char largeFileName[200];
    	sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    	printf("%s\n",largeFileName);
    	char filename[260];
    	sprintf(filename,"%s.",largeFileName);
    	char smallFileName[300];
    	char line[1080];
    	FILE *fp1, *fp2;
    	long sizeFile = file_size(largeFileName);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;
		fp1 = fopen(largeFileName, "r");
		if(fp1) {
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++) {
				accum = 0;
				sprintf(number,"%03d",i+1);
				sprintf(smallFileName, "%s%s", filename, number);
				if (segment_exists(smallFileName)) continue;
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			if (remove(largeFileName) != 0) printf("Gagal menghapus file asli: %s\n", largeFileName);
			else printf("File asli dihapus: %s\n", largeFileName);
		}
	}
    	printf("keluar\n");
}
```

In the beginning of the function, the variable chunks is used to count the number of small files that will be generated after splitting the large file. The variable i is used as an iterator in the loop, and the variable accum tracks the number of bytes that have been written to the small file during the splitting process. In the next line, the full path of the large file is formed by combining dirPath, enc2, and enc1. This path will be used to read the large file that will be split.

Next, the base name for the small files to be generated is formed by concatenating largeFileName and the "." symbol. This base name will be used to compose the names of the small files in each iteration. The variable smallFileName is used to store the name of the small file that will be generated in each iteration. The variable line is used to read and write data from the large file to the small file. The variables fp1 and fp2 are pointers that will be used to open the large file and the small file, respectively. The size of the large file is calculated by calling the file_size function, and the result is stored in the sizeFile variable.

Next, a check is performed to see if the size of the large file exceeds the specified minimum limit. If it does, the number of small files to be generated is calculated based on the size of the large file. The large file is opened in "r" (read) mode, and the names of the small files with three-digit sequential numbers are created.

In the loop section, data from the large file is read and written to the small file until it reaches the fixed size limit or the end of the large file. Data is written using the fwrite function. After the loop is completed, the small file is closed and the large file is deleted. If the deletion fails, an error message will be displayed.

### E. If a modular directory is renamed to become non-modular, then the contents of the directory will be intact again (fixed)
```c
void decode(const char *directoryPath, const char *prefix) {
    	DIR *dir;
    	struct dirent *entry;
    	char filePath[1000];
    	char mergedFilePath[1000];
   	FILE *mergedFile;
    	size_t prefixLength = strlen(prefix);
    	dir = opendir(directoryPath);
    	sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    	mergedFile = fopen(mergedFilePath, "w");

    	while ((entry = readdir(dir)) != NULL) {
        	if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            		sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            		FILE *file = fopen(filePath, "r");
            		if (strcmp(entry->d_name, prefix) == 0) {
                		fclose(file);
                		continue;
            		}
            		char buffer[1024];
            		size_t bytesRead;
            		while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                		fwrite(buffer, 1, bytesRead, mergedFile);
            		}
            		fclose(file);
        	}	 

    	}
    	closedir(dir);
    	fclose(mergedFile);
    	dir = opendir(directoryPath);

    	while ((entry = readdir(dir)) != NULL) {
        	if (entry->d_type == DT_REG) {
            		if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                		char extension[1000];
                		sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                		if (strstr(entry->d_name, extension) != NULL) {
                    			sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    			if (strcmp(filePath, mergedFilePath) != 0) {
                        			remove(filePath);
                    			}
                		}
            		}
        	}
    	}
    	closedir(dir);
}
```
The function is used to merge small files into one large file in a directory. The variable dir is used to store a pointer to the directory being processed, while the variable entry is used to read each entry in the directory. The variables filePath and mergedFilePath are used to store the full path of the small files and the large file to be merged. The variable mergedFile is a pointer used to open the large file in "write" mode.

First, the directory specified by the directoryPath parameter is opened using the opendir function. Next, the full path for the large file to be merged is formed by concatenating directoryPath and prefix. This path is then used to open the large file using the fopen function with "write" mode.

Next, a loop is performed to read each entry in the directory using readdir. For each entry, it is checked whether the entry is a regular file (DT_REG) and has a filename that matches the prefix. If so, the file is considered a small file to be merged. The full path for the current small file is formed and the file is opened in "read" mode using fopen. Within the loop, data is read from the small file using the fread function and written to the large file using the fwrite function. Data is read in a buffer of 1024 bytes (buffer) and written to the large file.

After the loop is finished, the current small file is closed. The directory and the large file are also closed after completion.

After that, the directory is opened again to process the next entries. Another loop is performed to read the entries in the directory. For each entry, it is checked whether the entry is a regular file and has a filename that matches the prefix. If so, the file is considered a small file that should not be merged. The extension for the small file is then formed by extracting the digits after the prefix and formatting it as a three-digit number. This extension is used to check whether the small file has been merged before. If it has, the file needs to be deleted. The full path for the current small file is formed and checked if it is the same as the path for the large file (mergedFilePath). If they are not the same, the small file is deleted using remove. After the loop is finished, the directory is closed again using closedir.
